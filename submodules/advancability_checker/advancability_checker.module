<?php

/* * ******************************************** Includes ************************************************* */


/* * ******************************************** Constants ************************************************* */
define('ADVANCABILITY_CHECKER_PATH', drupal_get_path('module', 'advancability_checker'));

/* * ******************************************** Permission hooks ************************************************* */

/**
 * Implementation of hook_permission().
 */
function advancability_checker_permission() {
  return array(
    'advancability check' => array(
      'title' => t('Use the accessibility check'),
      'description' => t('Use the Customization Accessibility module.'),
    )
  );
}

/* * ******************************************** Menu hooks ************************************************* */

/**
 * Implementation of hook_menu().
 */
function advancability_checker_menu() {
  $items[ADVANCABILITY_CONFIG_PATH . '/checker'] = array(
    'title' => 'Accessibility Checker',
    'type' => MENU_LOCAL_TASK,
    'description' => 'Advanced Accessibility Checker Preferences.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('advancability_checker_form'),
    'access arguments' => array('administer accessibility'),
    'file' => './advancability_checker.admin.inc'
  );

  return $items;
}

/* * ******************************************** Init hook ************************************************* */

/**
 * Implementation of hook_init().
 */
function advancability_checker_init() {
  global $theme;

  // return on admin theme
  if ($theme === variable_get('admin_theme')) {
    return;
  }

  // Load checker libraries if it's activated
  if (_advancability_checker_is_checker_enabled()) {
    _advancability_checker_load_chekcer();
  }
}

/**
 * Implements hook_jqmulti_libraries()
 * @return type
 */
function advancability_checker_jqmulti_libraries() {
  return array('quail');
}

/**
 * Implements hook_jqmulti_files()
 * @return type
 */
function advancability_checker_jqmulti_files() {
  /* Add the module's core JS files */
  $jqmulti_paths[] = drupal_get_path('module', 'advancability_checker') . '/js/advancability_checker.js';
  return $jqmulti_paths;
}

/**
 * Declares our variables.
 *
 * @param type $options
 * @return boolean
 */
function advancability_checker_variable_info($options) {
  $variables['advancability_guideline'] = array(
    'type' => 'string',
    'title' => t('Accessibility guidelines.', array(), $options),
    'default' => file_get_contents(ADVANCABILITY_CHECKER_PATH . '/guideline.json'),
    'description' => t('Accessibility guidelines.' . l(t('More info'), 'https://quail.readthedocs.org'), array(), $options),
  );
  
  return $variables;
}

/* * ******************************************** Theme Hooks ***************************************************** */
/*
 * Implements hook_theme();
 */

function advancability_checker_theme() {
  $templates = array(
    'advancability_checker' => array(
      'template' => 'advancability-checker', // define xxx-xxx.tpl.php inside module
      'path' => ADVANCABILITY_CHECKER_PATH . '/templates'
    )
  );

  return $templates;
}

/* * ******************************************* Block Hooks ****************************************************** */

/**
 * Implementation of hook_block_info().
 */
function advancability_checker_block_info() {
  $blocks = array();

  $blocks['advancability_checker'] = array(
    'info' => t('Accessibility Checker, added in advancability_block_info().'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );  

  return $blocks;
}

/**
 * Implementation of hook_block_view().
 */
function advancability_checker_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'advancability_checker':
      $block['subject'] = NULL;
      $block['content'] = array(
        '#markup' => theme('advancability_checker')
      );
      break;    
  }
  return $block;
}

/* * ******************************************** Helper functions ************************************************* */

/**
 * Helper function to load the checker library and related files
 */
function _advancability_checker_load_chekcer() {
  // Load jQuery UI dialog (comes with D7)
  drupal_add_library('system','ui.dialog');
  
  // Pass the guideline
  $guideline = (array) json_decode(variable_get('advancability_guideline'));
  drupal_add_js(array('advancability_checker' => array('guideline' => $guideline)), 'setting');

  drupal_add_js(libraries_get_path('quail') . '/quail.jquery.min.js', array(
    'weight' => 0
  ));

  drupal_add_js(ADVANCABILITY_CHECKER_PATH . '/js/advancability_checker.js', array(
    'weight' => 100
  ));
  
  drupal_add_css(ADVANCABILITY_CHECKER_PATH . '/css/advancability_checker.css');
}

/**
 * Helper function to check if the accessibility checker is enabled
 * @return boolean TRUE if enabled, FALSE otherwise
 */
function _advancability_checker_is_checker_enabled() {
  // Quail library is missing
  if (!libraries_get_path('quail')) {
    return FALSE;
  }

  // No access
  if (!user_access('advancability check')) {
    return FALSE;
  }

  return TRUE;
}
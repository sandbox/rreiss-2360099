<?php

/**
 * Checker Administration form
 */
function advancability_checker_form() {

  $form = array();
//  $form['advancability_checker_active'] = array(
//    '#type' => 'checkbox',
//    '#title' => t('Accessibility checker is Active'),
//    '#description' => t('Check to activate Accessibility checker.'),
//    '#default_value' => variable_get('advancability_checker_active', FALSE)
//  );

  $form['advancability_guideline'] = array(
    '#type' => 'textarea',
    '#title' => t('Accessibility guidelines.'),
    '#default_value' => variable_get('advancability_guideline', file_get_contents(ADVANCABILITY_CHECKER_PATH . '/guideline.json')),
    '#description' => t('Accessibility guideline (JSON).') . '<br>' . l(t('More info'), 'https://quail.readthedocs.org')
  );

  return system_settings_form($form);
}

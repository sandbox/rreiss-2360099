(function($) {

  Drupal.behaviors.advancabilityChecker = {
    attach: function(/*context, settings*/) {
      var isFirstTime = true;
      var testedSelector = '#page';
      var cachedResults = {};
      /**
       * @todo Add the excluded elements to the module admin page
       */
      var excludedElements = '.contextual-links-wrapper a, .tabs a, #main-content';

      if (Drupal.settings.advancability_checker === undefined || Drupal.settings.advancability_checker.guideline === undefined) {
        console.log('Guideline JSON is missing');
        return;
      }

      var guideline = Drupal.settings.advancability_checker.guideline;

      $('#accessibility-check').click(function() {
        if (!isFirstTime) {
//           clearCheckResults();
          showTestReport(cachedResults);
          return;
        }
        else {
          isFirstTime = false;
        }

        $(testedSelector).quail({
          jsonPath: '/sites/all/libraries/quail',
          guideline: guideline,
          preFilter: function(testName, element, options) {
            /* Filter excluded elements */
            if (element.is(excludedElements)) {
              return false;
            }
          },
          testFailed: function(event) {
            event.element.addClass('quail-result')
                    .addClass(event.severity);
            event.element.attr('data-quail', event.test.title.en);
            accessibilityTooltip(event);
          },
          complete: function(results) {
            cachedResults = results;
            showTestReport(results);
          }
        });
      });

      function accessibilityTooltip(event) {
        if (event !== undefined && event.test !== undefined &&
                event.test.title !== undefined && event.test.title.en !== undefined &&
                event.test.description !== undefined && event.test.description.en !== undefined
                ) {

          event.element.on('mouseover', function(e) {
            if ($(e.fromElement).hasClass('quail-tooltip'))
              return;

            var appendMarkup = '<strong>' + event.test.title.en + '</strong><p>' + event.test.description.en + '</p>';

            if ($('.quail-tooltip').length) {
              $('.quail-tooltip').append(appendMarkup);
            }
            else {
              $('body').append('<span class="quail-tooltip">' + appendMarkup + '</span>');
              $('.quail-tooltip').show();
            }
          });

          event.element.on('mouseleave', function(e) {
            if (!$(e.toElement).hasClass('quail-tooltip')) {
              $('.quail-tooltip').hide();
              $('.quail-tooltip').remove();
            }
          });

        }
      }

      function showTestReport(results) {
        // Don't reopen if already opened
        if ($('#accessibility-report').length) {
          return;
        }

        var reportMarkup = Drupal.t('No issues found');

        if (typeof (results.totals) !== 'undefined') {
          var resTotals = results.totals;
          /* Initalize the results before the acctual output */
          var sanitizedResults = {};
          sanitizedResults.severe = (typeof (resTotals.severe) !== 'undefined') ? resTotals.severe : '0';
          sanitizedResults.moderate = (typeof (resTotals.moderate) !== 'undefined') ? resTotals.moderate : '0';
          sanitizedResults.suggestion = (typeof (resTotals.suggestion) !== 'undefined') ? resTotals.suggestion : '0';
          reportMarkup = 'Severe errors: <strong class="severe">' + sanitizedResults.severe + '</strong>';
          reportMarkup += ' Moderate: <strong class="moderate">' + sanitizedResults.moderate + '</strong>';
          reportMarkup += ' Suggestions: <strong class="suggestion">' + sanitizedResults.suggestion + '</strong>';

          reportMarkup += '<ul>';
          $.each(results.results, function(key, value) {
            if (value.elements.length) {
              reportMarkup += '<li>(' + value.elements.length + ') ' + value.test.title.en + '</li>';
            }
          });
          reportMarkup += '</ul>';
        }

        jQuery('<div id="accessibility-report">' + reportMarkup + '</div>').dialog({
          title: Drupal.t('Accessibility Check Results'),
          minWidth: 600,
          close: function(event, ui) {
            // Remove the element when closing the dialog
            jQuery('#accessibility-report').remove();
          }
        });
      }
    }
  };

})(jQuery);

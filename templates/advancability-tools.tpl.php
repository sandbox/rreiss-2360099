<div role="button" id="toggle-accessibility-tools"><?php print t('Accessibility Tools'); ?></div>
<ul id="accessibility-tools-wrapper">
  <li id="reset-accessibility" role="button"><?php print t('Turn Off Accessibility tools'); ?></li>
  <?php
  $plugins = _advancability_get_tools();
  foreach ($plugins as $plugin_name => $plugin): ?>
    <?php if ($plugin['enabled']): ?>
      <li id="<?php print $plugin['id']; ?>"><?php print theme('advancability_plugin_' . $plugin_name); ?></li>
    <?php endif; ?>
  <?php endforeach; ?>
</ul>

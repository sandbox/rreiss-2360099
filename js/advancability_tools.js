(function($) {

  Drupal.behaviors.advancabilityTools = {
    attach: function(context, settings) {

      $('#block-cm-accessibility-cm-accessibility-tools').prependTo($('body'));
      $('#toggle-accessibility-tools').on('accessibilityOpened', openAccessibility);
      $('#toggle-accessibility-tools').on('accessibilityClosed', closeAccessibility);

      /* If the accessibility cookie exists - follow its data */
      if (typeof (jQuery.cookie) !== 'undefined' && jQuery.cookie('accessibility-mode') === '1') {
        $('#toggle-accessibility-tools').trigger('accessibilityOpened');
      }

      /* Listen to the toggle button click event */
      $('#toggle-accessibility-tools').on('click', function() {
        if ($(this).hasClass('active')) {
          $(this).trigger('accessibilityClosed');
        }
        else {
          $(this).trigger('accessibilityOpened');
        }
      });

      /* Add the reset functionality */
      $('#reset-accessibility').on('click', function() {
        $('#toggle-accessibility-tools').trigger('accessibilityClosed');
      });

      /**
       * Open the accessibility tools
       */
      function openAccessibility() {
        if (typeof (jQuery.cookie) !== 'undefined') {
          jQuery.cookie('accessibility-mode', 1, {path: '/'});
        }
        $('#accessibility-tools-wrapper').slideDown();
        $('#toggle-accessibility-tools').addClass('active');
        $('body').addClass('accessibility-mode');
      }

      /**
       * Close the accessibility tools
       */
      function closeAccessibility() {
        if (typeof (jQuery.cookie) !== 'undefined') {
          jQuery.cookie('accessibility-mode', '', {expires: -1, path: '/'});
        }
        $('#accessibility-tools-wrapper').slideUp();
        $('#toggle-accessibility-tools').removeClass('active');
        $('body').removeClass('accessibility-mode');
      }
    }
  };

})(jQuery);

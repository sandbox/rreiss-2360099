(function($) {

  /**
   * Implement the font resize (accessibility)
   */
  Drupal.behaviors.advancabilityTextMagnifier = {
    attach: function(context, settings) {

      /* Init the plugin */
      var selector = $('#region-content *');
      var isInUse = false;

      $('#toggle-accessibility-tools').on('accessibilityOpened', function() {
        //Establish original font sizes
        selector.each(function() {
          // Get the original font size and save it using jquery data
          var orgSize = parseInt($(this).css('font-size'));
          $(this).data('org-size', orgSize);
        });

        if (typeof (jQuery.cookie) !== 'undefined' && jQuery.cookie('accessibility-text-magnifier') !== null) {
          resizeFont(jQuery.cookie('accessibility-text-magnifier'));
        }
      });

      /* Handle close */
      $('#toggle-accessibility-tools').on('accessibilityClosed', resetTextMagnifier);

      //Bind resize function to AAA links
      $('#accessibility-plugin-text-magnifier .font-resize-link').on('click', resizeFont);

      function resetTextMagnifier() {
        if (isInUse) {
          $('#accessibility-plugin-text-magnifier .font-resize-link.reset').trigger('click');
          if (typeof (jQuery.cookie) !== 'undefined') {
            jQuery.cookie('accessibility-text-magnifier', '', {expires: -1, path: '/'}); // Delete the cookie
          }
        }
      }

      function resizeFont(size) {
        isInUse = true;
        var fontRatio = 1;

        // Use the function argument if exists
        if (typeof (size) !== 'string') {
          size = $(this).data('resize');
        }

        /* Set active class */
        $('#accessibility-plugin-text-magnifier .font-resize-link').removeClass('active').
                filter('.' + size).addClass('active');

        switch (size) {
          case 'reset':
            fontRatio = 1;
            break;
          case 'large':
            fontRatio = 1.15;
            break;
          case 'xlarge':
            fontRatio = 1.25;
            break;
        }

        /* Make the magic - magnify the texts */
        selector.each(function() {
          //Get the element's current and original font sizes
          var orgSize = parseInt($(this).data('org-size'));
          $(this).css('font-size', (orgSize * fontRatio) + 'px');
        });

        /* Save the current status to cookie */
        if (typeof (jQuery.cookie) !== 'undefined') {
          jQuery.cookie('accessibility-text-magnifier', size, {path: '/'});
        }
      }

    }
  };

})(jQuery);

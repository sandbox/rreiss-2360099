(function($) {

  Drupal.behaviors.advancabilityKeyboard = {
    attach: function(context, settings) {

      $('#keyboard-navigation-button').on('click', function() {
        $('#keyboard-navigation-message').modal({
          overlayClose: true,
          closeHTML: '<a id="accessibility-keyboard-modal-close" title="' + Drupal.t('Close') + '"></a>',
          overlayId: 'accessibility-keyboard-overlay',
          opacity: 75
        });
      });
    }
  };

})(jQuery);

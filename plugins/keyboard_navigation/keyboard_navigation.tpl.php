<span role="button" id="keyboard-navigation-button"><?php print t('Keyboard Navigation'); ?></span>
<div id="keyboard-navigation-message">
  <?php print t('You may skip between the website elements using the "TAB" key, 
Activate a link or button using the "Enter" key, 
Skip back to the previous element using the combination of SHIFT+TAB'); ?>
</div>
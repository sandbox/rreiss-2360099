(function($) {

  Drupal.behaviors.advancabilityZoom = {
    attach: function(context, settings) {

      /* Init the plugin */
      var isZoomLoaded = false;
      var PAGE_CONTENT = '#section-content';
      var clickables = $(PAGE_CONTENT).find('input[type="submit"], a, button');
      var lastClicked;
      var page = $('#page');

      $('#toggle-accessibility-tools').on('accessibilityOpened', loadZoomLibs);

      /* Close the plugin */
      $('#toggle-accessibility-tools').on('accessibilityClosed', resetZoomPlugin);

      /*
       * Helper function to load the libraries
       * @param {function} done Callback function to run after done loading
       * the libraries
       */
      function loadZoomLibs() {
        // Load the libraries only once
        if (isZoomLoaded)
          return;

        $.ajax({
          url: '/sites/all/libraries/jquery/jquery-' + settings.cm_accessibility.tools.jqmultiVersion + '.min.js',
          dataType: 'script',
          cache: true,
          success: function() {
            $.ajax({
              url: '/sites/all/libraries/jquery.zoomooz/jquery.zoomooz.min.js',
              dataType: 'script',
              cache: true,
              success: function() {
                $.ajax({
                  url: '/sites/all/modules/jqmulti/js/switch.js',
                  dataType: 'script',
                  cache: true,
                  success: function() {
                    isZoomLoaded = true;
                    $('#enable-zoom').show();
                    if (typeof (jQuery.cookie) !== 'undefined' && jQuery.cookie('accessibility-zoom') !== null) {
                      enableZoom();
                    }
                  }
                });
              }
            });
          }
        });
      }


      $('#enable-zoom').click(function() {
        if (!page.hasClass('zoom-enabled')) {
          enableZoom();
        }
        else {
          resetZoomPlugin();
        }
      });

      function enableZoom() {
        page.addClass('zoom-enabled');
        // Update the cookie
        if (typeof (jQuery.cookie) !== 'undefined') {
          jQuery.cookie('accessibility-zoom', '1', {path: '/'});
        }
        var zoomElements = $(PAGE_CONTENT + ' div');

        zoomElements.on('mouseover', function(e) {
          e.stopPropagation();
          if (!isZoomOn())
            return;

          $(this).addClass('zoom-area');

          $(this).on('click', function(e) {
            if (!isZoomOn())
              return;

            // Use the jqmulti jQuery object
            var jqMultiObject = window['jq' + settings.cm_accessibility.tools.jqmultiVersion.replace(/\./g, '')];
            jqMultiObject(this).zoomTo({
              targetsize: 0.5,
              root: page
            });
            e.stopPropagation();
          });
        });

        zoomElements.on('mouseout', function(e) {
          e.stopPropagation();

          if (isZoomOn()) {
            $(this).removeClass('zoom-area');
          }
        });
      }

      // Allow to zoom to the accessibility wrapper
      $('#accessibility-tools-wrapper').on('click', function(e) {
        resetZoom();
        e.stopPropagation();
      });

      /* Reset on esc */
      $(document).keyup(function(e) {
        if (e.keyCode === 27) {
          resetZoom();
          e.stopPropagation();
        }
      });

      function resetZoom() {
        var jqMultiObject = window['jq' + settings.cm_accessibility.tools.jqmultiVersion.replace(/\./g, '')];
        jqMultiObject('#page').zoomTo({root: page});
      }

      function resetZoomPlugin() {
        page.removeClass('zoom-enabled');
        resetZoom();
        /* Remove the cookie */
        if (typeof (jQuery.cookie) !== 'undefined') {
          jQuery.cookie('accessibility-zoom', '', {expires: -1, path: '/'});
        }
      }

      function isZoomOn() {
        return page.hasClass('zoom-enabled');
      }

      /* Ignore the 1st click on clickable items and react only on the second one */
      clickables.on('click', function(e) {
        if (isZoomOn()) {
          $(lastClicked).removeClass('zoom-clickable');
          if (lastClicked !== this) {
            e.preventDefault();
          }
          $(this).addClass('zoom-clickable');
          lastClicked = this;
        }
      });
    }
  };
})(jQuery);

<span role="button" class="color-link reset active" data-color="reset" title="<?php print t('Normal colors'); ?>">C</span>
<span role="button" class="color-link desaturate" data-color="desaturate" title="<?php print t('Grayscale'); ?>">C</span>
<span role="button" class="color-link invert" data-color="invert" title="<?php print t('High Contrast'); ?>">C</span>
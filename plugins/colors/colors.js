(function($) {

  Drupal.behaviors.advancabilityColors = {
    attach: function(context, settings) {

      /* Init the plugin */
      var isFirstColorSwitch = true;
      var isColorsLoaded = false;
      var textElements = $('#page').add($('#page *').not('img, canvas'));

      if (typeof (jQuery.cookie) !== 'undefined' && jQuery.cookie('accessibility-color') !== null) {
        loadLibraries(function() {
          applyColorEffect(jQuery.cookie('accessibility-color'));
        });
      }

      $('#toggle-accessibility-tools').on('accessibilityOpened', loadLibraries);

      /* Close the plugin */
      $('#toggle-accessibility-tools').on('accessibilityClosed', resetColorsPlugin);

      /*
       * Helper function to load the libraries
       * @param {function} done Callback function to run after done loading
       * the libraries
       */
      function loadLibraries(callback) {
        // Load the libraries just once
        if (isColorsLoaded)
          return;

        var tinyColor = $.ajax({
          url: '/sites/all/libraries/tinyColor/tinyColor.min.js',
          dataType: 'script',
          cache: true
        });

        var pixastic = $.ajax({
          url: '/sites/all/libraries/pixastic/pixastic.min.js',
          dataType: 'script',
          cache: true
        });

        /* Run the callback after all the scripts has been loaded */
        $.when(tinyColor, pixastic).done(function() {
          isColorsLoaded = true;
          if ($.isFunction(callback)) {
            callback();
          }
        });
      }

      $('.color-link').on('click', function() {
        /* Do nothing if already active */
        if ($(this).hasClass('active'))
          return;

        var colorEffect = $(this).data('color');
        applyColorEffect(colorEffect);
      });

      /* Apply the color effect to the page */
      function applyColorEffect(effect) {
        /* Apply active class */
        $('.color-link').removeClass('active').filter('.' + effect).addClass('active');

        applyImageEffect(effect);

        if (isFirstColorSwitch) {
          saveOrgColors();
          isFirstColorSwitch = false;
        }

        switch (effect) {
          case 'desaturate':
            greyscaleElements();
            break;
          case 'invert':
            highContrastElements();
            break;
          case 'reset':
            resetElementsColor();
            break;
        }

        /* Save the selection to cookie */
        if (typeof (jQuery.cookie) !== 'undefined') {
          jQuery.cookie('accessibility-color', effect, {path: '/'});
        }
      }

      /* Apply the choosen image effect */
      function applyImageEffect(effect) {
        // Iterate over the image elements
        $('img, canvas').each(function() {
          var img = this;
          var $img = $(this); // Get the jQuery object

          applyEffectIncHidden($img, function() {
            if($img.is('canvas')) { // Reset images with active effects (canvas items)
              Pixastic.revert(img);
              img = img.__pixastic_org_image; // Replace the img object with the original image item
            }

            if (effect !== 'reset') { // Apply the new effect
              Pixastic.process(img, effect);
            }
          });
        });
      }

      /**
       * Helper function to apply the image effect on hidden elements
       * as well
       * @param img The image object
       * @param effect The effect callback function
       */
      function applyEffectIncHidden(img, effect) {
        // Do nothing for visible items
        if (img.is(':visible')) {
          effect();
          return;
        }

        // Check for changes in the elements visiblity every 250 ms
        var slideshowsChecker = setInterval(function() {
          if (img.is(':visible')) {
            clearInterval(slideshowsChecker);
            // Now it's visible -> apply the effect
            effect();
          }
        }, 250);
      }

      function greyscaleElements() {
        resetElementsColor();

        textElements.each(function() {
          var elem = $(this);

          var curColors = {
            'color': $(this).css('color'),
            'border-color': $(this).css('border-color'),
            'background-color': $(this).css('background-color')
          };
          $.each(curColors, function(attr, color) {
            var newColor = tinycolor(color).greyscale().toString();
            elem.css(attr, newColor);
          });
        });
      }

      function highContrastElements() {
        var newColors = {
          'color': '#FFF',
          'border-color': '#5BA0D0',
          'background-color': '#000'
        };

        textElements.each(function() {
          switchColors($(this), newColors);
        });
      }

      function resetElementsColor() {
        textElements.each(function() {
          var elem = $(this);
          // Remove the inline styles
          elem.css('background-color', '').
                  css('color', '').
                  css('border-color', '');
        });
      }

      function saveOrgColors() {
        textElements.each(function() {
          var elem = $(this);
          elem.data('org-color', elem.css('color'));
          elem.data('org-border-color', elem.css('border-color'));
          elem.data('org-background-color', elem.css('background-color'));
        });
      }

      function switchColors(elem, newColors) {
        $.each(newColors, function(attr, color) {
          $(elem).css(attr, color);
        });
      }

      function resetColorsPlugin() {
        /* Remove the active class */
        $('.color-link').removeClass('active');

        /* Reset the color effects */
        applyImageEffect('reset');
        resetElementsColor();

        /* Remove the cookie */
        if (typeof (jQuery.cookie) !== 'undefined') {
          jQuery.cookie('accessibility-color', '', {expires: -1, path: '/'});
        }
      }
    }
  };

})(jQuery);

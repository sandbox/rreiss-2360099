(function($) {

  Drupal.behaviors.advancabilityStatement = {
    attach: function(context, settings) {

//      $('#toggle-accessibility-tools').on('accessibilityOpened', openAccessibility);
//      $('#toggle-accessibility-tools').on('accessibilityClosed', closeAccessibility);
      $('#accessibility-statement-button').on('click', function() {
        $('#accessibility-statement-message').modal({
          overlayClose: true,
          closeHTML: '<a id="accessibility-modal-close" title="' + Drupal.t('Close') + '"></a>',
          overlayId: 'accessibility-statement-overlay',
          opacity: 75
        });
      });
    }
  };

})(jQuery);

<span role="button" id="accessibility-statement-button"><?php print t('Accessibility statement'); ?></span>
<div id="accessibility-statement-message">
  <h2><?php print t('Accessibility statement'); ?></h2>
  <?php print t('
  <p>We want everyone who visits the @site_name website to feel welcome and find the experience rewarding.</p> 
  <h3>What are we doing?</h3>
  <p>To help us make the @site_name website a positive place for everyone, we\'ve been using the <a href="http://www.w3.org/TR/WCAG/" target="_blank">Web Content Accessibility Guidelines (WCAG) 2.0</a>. These guidelines explain how to make web content more accessible for people with disabilities, and user friendly for everyone.</p>    
  <p>The guidelines have three levels of accessibility (A, AA and AAA). We’ve chosen Level AA as the target for the @site_name website.</p>
  <h3>How are we doing?</h3>
  <p>We\'ve worked hard on the @site_name website and believe we\'ve achieved our goal of Level AA accessibility. We monitor the website regularly to maintain this, but if you do find any problems, please get in touch.</p>
  <p>This accessibility statement was generated on 7th August 2014 using the <a href="http://accessibilitystatementgenerator.com" target="_blank">Accessibility Statement Generator</a>.</p>  
  ', array('@site_name' => variable_get('site_name',''))); ?>
</div>
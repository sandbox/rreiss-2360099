<?php

/**
 * Checker Administration form
 */
function advancability_dashboard() {

  $form = array();

  $dashboard_markup = '';
  $dashboard_markup .= '<strong>' . t('Active Submodules:') . '</strong><ul>';
  $dashboard_markup .= module_exists('advancability_checker') ? '<li>Advancability Checker</li>' : '';
  $dashboard_markup .= module_exists('advancability_tools') ? '<li>Advancability Tools</li>' : '';
  $dashboard_markup .= '</ul>';
  
  $form['advancability_dashboard'] = array(
    '#markup' => $dashboard_markup
  );

  return $form;
}

function advancability_tools() {
  $form = array();

  $form['advancability_tools_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Accessibility tools is Active'),
    '#description' => t('Check to activate Accessibility tools.'),
    '#default_value' => variable_get('advancability_tools_active', FALSE)
  );

  $form['plugins_vtabs'] = array(
    '#type' => 'vertical_tabs'
  );

  $form['advancability_plugins'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plugins'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'plugins_vtabs'
  );

  _advancability_add_tools_js_checkboxes($form);

  return system_settings_form($form);
}

function advancability_create_plugin() {
    $form['advancability_plugins']['create_plugin_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Plugin Name'),
    '#size' => 45,
    '#max_length' => 45,
    '#required' => TRUE
  );

  $form['advancability_plugins']['create_plugin_button'] = array(
    '#type' => 'submit',
    '#value' => t('Add Plugin'),
    '#submit' => array('advancability_create_plugin_submit')
  );

  return $form;
}

function advancability_create_plugin_submit($form, &$form_state) {
  $plugin_name = $form_state['values']['create_plugin_name'];
  $dir_name = CM_ACCESSIBILTY_PLUGINS_PATH . '/' . $plugin_name;

  if (!drupal_mkdir($dir_name)) {
    form_set_error('create_plugin_name', t('Unable to create the directory'));
    return $form;
  }

  $formats_to_create = array('.js', '.tpl.php', '.css');
  foreach ($formats_to_create as $format) {
    $file_name = $dir_name . '/' . $plugin_name . $format;
    $file_handler = fopen($file_name, 'w') or form_set_error('create_plugin_name', t('Unable to create') . ' ' . $file_name);
    fclose($file_handler);
  }

  drupal_set_message(t('Plugin created successfully'));
}

function _advancability_add_tools_js_checkboxes(&$form) {

  $js_tools = _advancability_get_tools();

  if (empty($js_tools)) {
    return FALSE;
  }

  foreach ($js_tools as $name => $path) {
    $form['advancability_plugins']['advancability_' . $name . '_active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable ' . $name),
      '#default_value' => variable_get('advancability_' . $name . '_active', FALSE)
    );
  }
}

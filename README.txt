README.txt
==========

A module with one purpose - advance your site accessibility.
The module has two submodules:
* Advancability Tools - Advanced accessibility tools for your users (usually from all roles)
* Advancibility Checker - Help developers, themers, site builders and content managers to build more accessible site.
The module provides errors, warnings and suggestions in order to make the site more accessible for your users.

INSTALLATION
======================
1. Advancibility Checker
  1.1 Download quailjs from http://quailjs.org/
  1.2 Extract the archive's content
  1.3 Create new directory named 'quail' under sites/all/libraries
  1.4 Add the following files to the newly create directory: (It should be found on 'dist' directory of the extracted archive)
    * quail.jquery.min.js
    * tests.json
  1.5 Place Advancability Checker block in one of the site's regions (admin/structure/block)



AUTHOR/MAINTAINER
======================
-Rotem Reiss <reissr at gmail DOT com>
http://dofinity.com
